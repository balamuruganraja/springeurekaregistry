FROM maven:3.5.2-jdk-8-alpine AS MAVEN_TOOL_CHAIN
COPY pom.xml /tmp/
COPY src /tmp/src/
WORKDIR /tmp/
RUN mvn package
RUN cd target && ls
EXPOSE 8761
ENTRYPOINT ["java","-jar","target/SpringEurekaRegistry-0.0.1-SNAPSHOT.jar"]
